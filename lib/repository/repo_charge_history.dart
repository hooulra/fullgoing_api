import 'package:dio/dio.dart';
import 'package:full_going_admin_app/config/config_api.dart';
import 'package:full_going_admin_app/functions/token_lib.dart';
import 'package:full_going_admin_app/model/my_charge_list_result.dart';



class RepoChargeHistory {
  // 다트문법. 페이지 언급안하면 기본값 1 주고싶을때.. {} 여기 안에 이렇게 쓰면 됨.
  // 자바에선 안됨.. 다트가 더 늦게 나온 언어라서 기존 언어 단점이 보완된거임..불편하니까..
  Future<MyChargeListResult> getList({int page = 1}) async {
    final String _baseUrl = '$apiUri/charging-history/my/charges?page={page}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(_baseUrl.replaceAll('{page}', page.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return MyChargeListResult.fromJson(response.data);

  }
}