import 'package:flutter/material.dart';
import 'package:full_going_admin_app/components/common/component_appbar_actions.dart';
import 'package:full_going_admin_app/components/common/component_margin_horizon.dart';
import 'package:full_going_admin_app/components/common/component_margin_vertical.dart';
import 'package:full_going_admin_app/components/component_admin_main_button.dart';
import 'package:full_going_admin_app/config/config_color.dart';
import 'package:full_going_admin_app/config/config_size.dart';
import 'package:full_going_admin_app/config/config_style.dart';
import 'package:full_going_admin_app/enums/enum_size.dart';
import 'package:full_going_admin_app/functions/token_lib.dart';
import 'package:full_going_admin_app/page/menu/board/page_board.dart';
import 'package:full_going_admin_app/page/menu/board/page_board_list.dart';
import 'package:full_going_admin_app/page/menu/kickBoard/page_kick_board.dart';
import 'package:full_going_admin_app/page/menu/kickBoard/page_kick_board_list.dart';
import 'package:full_going_admin_app/page/menu/kickBoard/page_kick_board_update.dart';
import 'package:full_going_admin_app/page/menu/member/page_member.dart';
import 'package:full_going_admin_app/page/menu/member/page_member_list.dart';
import 'package:full_going_admin_app/page/menu/page_profile_fix.dart';

class PageHome extends StatefulWidget {
  const PageHome({super.key});

  @override
  State<PageHome> createState() => _PageHomeState();
}

class _PageHomeState extends State<PageHome> {
  _ShowDialog({
    required String titleText,
    required Color titleColor,
    required String contentText,
    required Color contentColor,
    required VoidCallback callbackOk,
    required VoidCallback callbackCancel,
  }) {
    showDialog(
        context: context,
        //barrierDismissible - Dialog를 제외한 다른 화면 터치 x
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            // RoundedRectangleBorder - Dialog 화면 모서리 둥글게 조절
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            //Dialog Main Title
            title: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(Icons.report_problem_outlined, color: titleColor),
                ComponentMarginHorizon(enumSize: EnumSize.mid),
                Text(titleText,
                    style: TextStyle(
                        color: titleColor,
                        fontSize: fontSizeBig,
                        fontWeight: FontWeight.bold)),
              ],
            ),
            content: Text(contentText,
                style: TextStyle(
                    color: contentColor,
                    fontSize: fontSizeMid,
                    fontWeight: FontWeight.bold)),
            actions: [
              TextButton(
                child: Text("확인"),
                onPressed: callbackOk,
              ),
              TextButton(
                child: Text("취소"),
                onPressed: callbackCancel,
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarActions(title: "관리자"),
      body: ListView(
        children: [
          const ComponentMarginVertical(),
          Container(
            padding: bodyPaddingLeftRight,
            child: Column(
              children: [
                const ComponentMarginVertical(enumSize: EnumSize.big),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ComponentAdminMainButton(
                      buttonText: "비밀번호 변경",
                      voidCallback: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    const PageProfileFix()));
                      },
                    ),
                    ComponentAdminMainButton(
                      buttonText: "로그아웃",
                      voidCallback: () {
                        _ShowDialog(
                          titleText: "로그아웃",
                          titleColor: colorRed,
                          contentText: "정말 로그아웃을 하시겠습니까?",
                          contentColor: colorDarkGray,
                          callbackOk: () {
                            TokenLib.logout(context);
                          },
                          callbackCancel: () {
                            Navigator.pop(context);
                          },
                        );
                      },
                    ),
                  ],
                ),
                const ComponentMarginVertical(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ComponentAdminMainButton(
                      buttonText: "킥보드 등록",
                      voidCallback: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    PageKickBoard()));
                        //  PageKickBoardUpdate()));
                      },
                    ),
                    ComponentAdminMainButton(
                      buttonText: "킥보드 리스트",
                      voidCallback: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    PageKickBoardList()));
                      },
                    ),
                  ],
                ),
                const ComponentMarginVertical(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ComponentAdminMainButton(
                      buttonText: "회원 등록",
                      voidCallback: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    PageMember()));

                      },
                    ),
                    ComponentAdminMainButton(
                      buttonText: "회원 리스트",
                      voidCallback: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    PageMemberList()));

                      },
                    ),
                  ],
                ),
                const ComponentMarginVertical(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ComponentAdminMainButton(
                      buttonText: "후기 등록",
                      voidCallback: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    PageBoard()));
                      },
                    ),
                    ComponentAdminMainButton(
                      buttonText: "후기 리스트",
                      voidCallback: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    PageBoardList()));

                      },
                    ),
                  ],
                ),
                const ComponentMarginVertical(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
