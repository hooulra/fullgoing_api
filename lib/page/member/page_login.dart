import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:full_going_admin_app/components/common/component_custom_loading.dart';
import 'package:full_going_admin_app/components/common/component_margin_vertical.dart';
import 'package:full_going_admin_app/components/common/component_notification.dart';
import 'package:full_going_admin_app/components/common/component_text_btn.dart';
import 'package:full_going_admin_app/config/config_color.dart';
import 'package:full_going_admin_app/config/config_form_formatter.dart';
import 'package:full_going_admin_app/config/config_form_validator.dart';
import 'package:full_going_admin_app/config/config_size.dart';
import 'package:full_going_admin_app/config/config_style.dart';
import 'package:full_going_admin_app/enums/enum_size.dart';
import 'package:full_going_admin_app/functions/token_lib.dart';
import 'package:full_going_admin_app/middleware/middleware_login_check.dart';
import 'package:full_going_admin_app/model/login_request.dart';
import 'package:full_going_admin_app/page/loginWidget/wave_widget.dart';
import 'package:full_going_admin_app/page/member/page_find_password.dart';
import 'package:full_going_admin_app/page/member/page_join.dart';
import 'package:full_going_admin_app/repository/repo_member.dart';

class PageLogin extends StatefulWidget {
  const PageLogin({super.key});

  @override
  State<PageLogin> createState() => _PageLogin();
}

class _PageLogin extends State<PageLogin> {
  final _formKey = GlobalKey<FormBuilderState>();
  final double _suffixIconSize = 18;
  bool _isVisible = true;
  late ScrollController _controller;

  Future<void> _doLogin(LoginRequest loginRequest) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMember().doLogin(loginRequest).then((res) {
      BotToast.closeAllLoading();

      // api에서 받아온 결과값을 token에 넣는다.
      TokenLib.setMemberToken(res.data.token);
      TokenLib.setMemberName(res.data.name);

      // 미들웨어에게 부탁해서 토큰값 여부 검사 후 페이지 이동을 부탁한다.
      MiddlewareLoginCheck().check(context);
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '로그인 실패',
        subTitle: '아이디 혹은 비밀번호를 확인해주세요.',
      ).call();
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    _controller = ScrollController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return KeyboardVisibilityBuilder(
      builder: (context, isKeyboardVisible) {
        return Scaffold(
          body: SingleChildScrollView(
            child: Column(children: [
              Stack(
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height / 2,
                    color: Colors.lightBlue,
                  ),
                  AnimatedPositioned(
                    duration: const Duration(milliseconds: 500),
                    curve: Curves.easeOutQuad,
                    top: isKeyboardVisible
                        ? -MediaQuery.of(context).size.height / 5
                        : 0.0,
                    child: WaveWidget(
                      size: MediaQuery.of(context).size,
                      yOffset: MediaQuery.of(context).size.height / 3.0 + 50,
                      color: colorBackGround,
                    ),
                  ),
                  Column(
                    children: [
                      Container(
                        padding: EdgeInsets.zero,
                        width: MediaQuery.of(context).size.width,
                        //height: MediaQuery.of(context).size.height / 5,
                        //decoration: BoxDecoration(color: Colors.red),
                        alignment: Alignment.center,
                        child: Image.asset(
                          'assets/img_splash.png',
                          color: Colors.black,
                          fit: BoxFit.fill,
                        ),
                      ),
                      const ComponentMarginVertical(enumSize: EnumSize.big),
                      Container(
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.only(left: 30),
                        child: Text(
                          'Login to Admin',
                          style: TextStyle(
                            fontSize: 40.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      ComponentMarginVertical(),
                    ],
                  ),
                ],
              ), //  물결/로고 이미지 및 로그인 텍스트
              FormBuilder(
                key: _formKey,
                child: Container(
                  padding: bodyPaddingLeftRight,
                  child: Column(children: [
                    FormBuilderTextField(
                      name: 'username',
                      maxLength: 20,
                      inputFormatters: [maskNoInputKrFormatter],
                      decoration: const InputDecoration(
                        labelText: '아이디',
                        counterText: "",
                        labelStyle: TextStyle(color: colorPrimary),
                        filled: true,
                        enabledBorder: UnderlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          borderSide: BorderSide.none,
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                        ),
                        prefixIcon: Icon(Icons.account_circle_outlined,
                            color: colorPrimary, size: 20),
                      ),
                      validator: validatorOfLoginUsername,
                    ),
                    const ComponentMarginVertical(),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        FormBuilderTextField(
                          name: 'password',
                          inputFormatters: [maskNoInputKrFormatter],
                          maxLength: 20,
                          decoration: InputDecoration(
                            labelText: '비밀번호',
                            labelStyle: TextStyle(color: colorPrimary),
                            filled: true,
                            counterText: "",
                            enabledBorder: UnderlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              borderSide: BorderSide.none,
                            ),
                            border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                            ),
                            prefixIcon: Icon(Icons.lock_outline,
                                color: colorPrimary, size: 20),
                            suffixIcon: GestureDetector(
                              child: _isVisible
                                  ? Icon(Icons.visibility_off,
                                      size: _suffixIconSize,
                                      color: colorPrimary)
                                  : Icon(Icons.visibility,
                                      size: _suffixIconSize,
                                      color: colorPrimary),
                              onTap: () {
                                setState(() {
                                  _isVisible = !_isVisible;
                                });
                              },
                            ),
                          ),
                          obscureText: _isVisible,
                          validator: validatorOfLoginPassword,
                        ),
                        const ComponentMarginVertical(),
                        GestureDetector(
                          child: Text(
                            '비밀번호 찾기',
                            style: TextStyle(
                                fontSize: fontSizeSm,
                                color: colorPrimary,
                                fontWeight: FontWeight.w600),
                          ),
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        const PageFindPassword()));
                          },
                        )
                      ],
                    ),
                    const ComponentMarginVertical(enumSize: EnumSize.bigger),
                    Container(
                      padding: contentPaddingSmButton,
                      width: MediaQuery.of(context).size.width,
                      child: ComponentTextBtn(
                        text: "로그인",
                        textColor: Colors.white,
                        bgColor: colorPrimary,
                        borderColor: colorPrimary,
                        callback: () {
                          if (_formKey.currentState?.saveAndValidate() ??
                              false) {
                            BotToast.showLoading();
                            LoginRequest loginRequest = LoginRequest(
                              _formKey.currentState!.fields['username']!.value,
                              _formKey.currentState!.fields['password']!.value,
                            );
                            _doLogin(loginRequest);
                          }
                          debugPrint(_formKey.currentState?.value.toString());
                        },
                      ),
                    ),
                  ]),
                ),
              ),
            ]),
          ),
        );
      },
    );
  }
}
