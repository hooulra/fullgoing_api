import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:full_going_admin_app/components/common/component_appbar_popup.dart';
import 'package:full_going_admin_app/components/common/component_custom_loading.dart';
import 'package:full_going_admin_app/components/common/component_margin_horizon.dart';
import 'package:full_going_admin_app/components/common/component_margin_vertical.dart';
import 'package:full_going_admin_app/components/common/component_notification.dart';
import 'package:full_going_admin_app/components/common/component_text_btn.dart';
import 'package:full_going_admin_app/config/config_color.dart';
import 'package:full_going_admin_app/config/config_form_formatter.dart';
import 'package:full_going_admin_app/config/config_size.dart';
import 'package:full_going_admin_app/config/config_style.dart';
import 'package:full_going_admin_app/enums/enum_size.dart';
import 'package:full_going_admin_app/model/admin_member/member_detail_item_result.dart';
import 'package:full_going_admin_app/model/remain_price_request.dart';
import 'package:full_going_admin_app/repository/repo_amount.dart';
import 'package:full_going_admin_app/repository/repo_member_history.dart';

class PageChargeAmount extends StatefulWidget {
  const PageChargeAmount({super.key, required this.memberId});

  final int memberId;

  @override
  State<PageChargeAmount> createState() => _PageChargeAmountState();
}

class _PageChargeAmountState extends State<PageChargeAmount> {
  num remainPrice = 0;

  Future<void> _loadProfileDetailData() async {
    MemberDetailItemResult result =
        await RepoMemberHistory().getMemberDetail(widget.memberId);
    setState(() {
      remainPrice = result.data.remainPrice;
    });
  }

  Future<void> _doRemainPriceOfMember(
      int memberId, RemainPriceRequest remainPriceRequest) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoAmount()
        .doChargeForMember(memberId, remainPriceRequest)
        .then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '금액 충전 완료',
        subTitle: '금액 충전이 완료되었습니다.',
      ).call();

      Navigator.pop(context);
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '금액 충전 실패',
        subTitle: '고객센터로 문의주세요.',
      ).call();
    });
  }

  @override
  void initState() {
    super.initState();
    _loadProfileDetailData();
  }

  // 변수
  double money = 0;
  double resultMoney = 0;

  // 함수
  _incrementCounter(double amount) {
    setState(() {
      this.money += amount;
    });
  }

  _resetCounter() {
    setState(() {
      this.money = 0;
    });
  }

  _chargeShowDialog(
      {required String title,
      required VoidCallback callbackCharge,
      required VoidCallback callbackCancel,
      String? content}) {
    showDialog(
        context: context,
        //barrierDismissible - Dialog를 제외한 다른 화면 터치 x
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            // RoundedRectangleBorder - Dialog 화면 모서리 둥글게 조절
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            //Dialog Main Title
            title: Text(title,
                style: TextStyle(
                    fontSize: fontSizeBig, fontWeight: FontWeight.bold)),
            actions: <Widget>[
              TextButton(
                child: Text("충전"),
                onPressed: callbackCharge,
              ),
              TextButton(
                child: Text("취소"),
                onPressed: callbackCancel,
              ),
            ],
          );
        });
  }

  _paymentAmountButton(double value) {
    return Container(
      margin: EdgeInsets.only(top: 5, bottom: 5),
      width: MediaQuery.sizeOf(context).width / 4,
      child: FilledButton(
        onPressed: () => _incrementCounter(value),
        child: Text('+' + maskWonFormatter.format(value), style: TextStyle(fontSize: fontSizeSm)),
        style: ButtonStyle(
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)))),
      ),
    );
  }

  // 빌드
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(
        title: '금액충전',
      ),
      body: SingleChildScrollView(
          child: Container(
        padding: bodyPaddingLeftRight,
        child: Column(
          children: [
            const ComponentMarginVertical(enumSize: EnumSize.big),
            Container(
              child: Row(
                children: [
                  Icon(Icons.monetization_on_rounded),
                  const ComponentMarginHorizon(enumSize: EnumSize.mid),
                  Text('충전금액', style: TextStyle(fontSize: fontSizeBig)),
                ],
              ),
            ),
            const ComponentMarginVertical(enumSize: EnumSize.mid),
            Container(
              margin: EdgeInsets.only(left: 50, right: 50),
              padding: EdgeInsets.only(left: 20),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                border: Border.all(color: colorDarkGray),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      child: Text(
                    maskWonFormatter.format(money),
                    style: TextStyle(fontSize: 20),
                  )),
                  IconButton(
                    onPressed: () => _resetCounter(),
                    icon: Icon(Icons.cancel, color: colorRed),
                  ),
                ],
              ),
            ),
            const ComponentMarginVertical(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _paymentAmountButton(1000),
                _paymentAmountButton(3000),
                _paymentAmountButton(5000),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _paymentAmountButton(10000),
                _paymentAmountButton(30000),
                _paymentAmountButton(50000),
              ],
            ),
            const ComponentMarginVertical(enumSize: EnumSize.big),
            Padding(
              padding: EdgeInsets.only(left: 40, right: 40),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('충전 후 금액 :',
                      style: TextStyle(
                          fontSize: fontSizeBig, color: colorPrimary)),
                  Text(
                    "₩ " + maskWonFormatter.format(money + remainPrice),
                    style: TextStyle(fontSize: fontSizeBig),
                  ),
                ],
              ),
            ),
            const ComponentMarginVertical(enumSize: EnumSize.big),
            Container(
              child: Row(
                children: [
                  Icon(Icons.credit_card, size: 30),
                  const ComponentMarginHorizon(enumSize: EnumSize.mid),
                  Text('결제 전 주의사항', style: TextStyle(fontSize: fontSizeSuper)),
                ],
              ),
            ),
            const ComponentMarginVertical(),
            Container(
              padding: EdgeInsets.only(left: 20, right: 20),
              width: MediaQuery.sizeOf(context).width,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                      '※ 자동 충전 금액은 최소 1,000캐시 ~ 최대 50,000 캐시까지 1천 캐시 단위로 입력 가능합니다.',
                      style: TextStyle(fontSize: fontSizeMid)),
                  const ComponentMarginVertical(enumSize: EnumSize.small),
                  Text('※ 최초 1회 결제 이후에는 회원님이 설정한 조건대로 결제가 진행됩니다.',
                      style: TextStyle(fontSize: fontSizeMid)),
                  const ComponentMarginVertical(enumSize: EnumSize.small),
                  Text('※ 결제 카드를 변경할 시에는 해지 후 카드를 재설정 하셔야 변경이 가능합니다.',
                      style: TextStyle(fontSize: fontSizeMid)),
                  const ComponentMarginVertical(enumSize: EnumSize.small),
                ],
              ),
            ),
            const ComponentMarginVertical(enumSize: EnumSize.bigger),
            Container(
              width: MediaQuery.of(context).size.width,
              child: ComponentTextBtn(
                iconData: Icons.check_circle,
                text: "충전하기",
                textColor: Colors.white,
                borderColor: colorGreen,
                bgColor: colorGreen,
                callback: () {
                  _chargeShowDialog(
                    title: maskWonFormatter.format(money) + " 원을 충전 합니다.",
                    callbackCharge: () {
                      RemainPriceRequest remainPriceRequest =
                          RemainPriceRequest(money);
                      _doRemainPriceOfMember(
                          widget.memberId, remainPriceRequest);
                      Navigator.pop(context);
                    },
                    callbackCancel: () {
                      Navigator.pop(context);
                    },
                  );
                },
              ),
            ),
          ],
        ),
      )),
    );
  }
}
