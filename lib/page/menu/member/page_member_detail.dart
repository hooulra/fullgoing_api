import 'package:flutter/material.dart';
import 'package:full_going_admin_app/components/common/component_appbar_popup.dart';
import 'package:full_going_admin_app/components/common/component_margin_vertical.dart';
import 'package:full_going_admin_app/components/common/component_text_btn.dart';
import 'package:full_going_admin_app/config/config_color.dart';
import 'package:full_going_admin_app/config/config_form_formatter.dart';
import 'package:full_going_admin_app/config/config_size.dart';
import 'package:full_going_admin_app/config/config_style.dart';
import 'package:full_going_admin_app/enums/enum_size.dart';
import 'package:full_going_admin_app/model/admin_member/member_detail_item_result.dart';
import 'package:full_going_admin_app/page/menu/member/page_charge_amount.dart';
import 'package:full_going_admin_app/page/menu/member/page_full_going_pass.dart';
import 'package:full_going_admin_app/repository/repo_member_history.dart';

class PageMemberDetail extends StatefulWidget {
  const PageMemberDetail({super.key, required this.memberId});

  final int memberId;

  @override
  State<PageMemberDetail> createState() => _PageMemberDetailState();
}

class _PageMemberDetailState extends State<PageMemberDetail> {
  String dateCreate = "";
  bool isLicence = false;
  String name = "";
  String phoneNumber = "";
  num remainMileage = 0;
  num remainPass = 0;
  num remainPrice = 0;
  String username = "";

  Future<void> _loadProfileDetailData() async {
    MemberDetailItemResult result =
        await RepoMemberHistory().getMemberDetail(widget.memberId);
    setState(() {
      dateCreate = result.data.dateCreate;
      isLicence = result.data.isLicence;
      name = result.data.name;
      phoneNumber = result.data.phoneNumber;
      remainMileage = result.data.remainMileage;
      remainPass = result.data.remainPass;
      remainPrice = result.data.remainPrice;
      username = result.data.username;
    });
  }

  @override
  void initState() {
    super.initState();
    _loadProfileDetailData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(
        title: '회원 상세 정보',
      ),
      body: Container(
        padding: bodyPaddingLeftRight,
        child: Column(
          children: [
            ComponentMarginVertical(
              enumSize: EnumSize.big,
            ),
            Text("회원 상세 정보",
                style: TextStyle(
                    fontSize: fontSizeSuper, fontWeight: FontWeight.bold)),
            ComponentMarginVertical(
              enumSize: EnumSize.big,
            ),
            _buildInfoRow("회원가입 날짜", dateCreate),
            ComponentMarginVertical(),
            _buildInfoRow("아이디", username),
            ComponentMarginVertical(),
            _buildInfoRow("이름", name),
            ComponentMarginVertical(),
            _buildInfoRow("휴대폰 번호", phoneNumber),
            ComponentMarginVertical(),
            _buildInfoRow("잔여 금액", maskWonFormatter.format(remainPrice) + ' 원'),
            ComponentMarginVertical(),
            _buildInfoRow("잔여 패스(분)", '${remainPass}'),
            ComponentMarginVertical(),
            _buildInfoRow("잔여 마일리지", maskNumFormatter.format(remainMileage)),
            ComponentMarginVertical(),
            _buildInfoRow("면허 등록 유무", isLicence ? "등록" : "미등록"),
            ComponentMarginVertical(
              enumSize: EnumSize.bigger,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: ComponentTextBtn(
                iconData: Icons.electric_scooter_outlined,
                text: "패스 충전",
                textColor: Colors.white,
                borderColor: colorPrimary,
                bgColor: colorPrimary,
                callback: () {
                  Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  PageFullGoingPass(memberId: widget.memberId)))
                      .then(
                    (value) {
                      setState(() {
                        _loadProfileDetailData();
                      });
                    },
                  );
                },
              ),
            ),
            ComponentMarginVertical(),
            Container(
              width: MediaQuery.of(context).size.width,
              child: ComponentTextBtn(
                iconData: Icons.payment_outlined,
                text: "금액 충전",
                textColor: Colors.white,
                borderColor: colorPrimary,
                bgColor: colorPrimary,
                callback: () {
                  Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  PageChargeAmount(memberId: widget.memberId)))
                      .then(
                    (value) {
                      setState(() {
                        _loadProfileDetailData();
                      });
                    },
                  );
                },
              ),
            ),
            ComponentMarginVertical(),
            Container(
              width: MediaQuery.of(context).size.width,
              child: ComponentTextBtn(
                iconData: Icons.check_circle,
                text: "확인",
                textColor: Colors.white,
                borderColor: colorPrimary,
                bgColor: colorPrimary,
                callback: () {
                  Navigator.pop(context);
                },
              ),
            ),
            Spacer(flex: 2),
          ],
        ),
      ),
    );
  }

  Widget _buildInfoRow(String title, String value, {bool isHighlight = false}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(title,
            style: TextStyle(
                fontSize: fontSizeMid,
                color: isHighlight ? colorPrimary : colorDarkGray)),
        Text(value,
            style:
                TextStyle(fontSize: fontSizeMid, fontWeight: FontWeight.bold)),
      ],
    );
  }
}
