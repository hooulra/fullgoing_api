import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:full_going_admin_app/components/common/component_appbar_popup.dart';
import 'package:full_going_admin_app/components/common/component_custom_loading.dart';
import 'package:full_going_admin_app/components/common/component_margin_vertical.dart';
import 'package:full_going_admin_app/components/common/component_notification.dart';
import 'package:full_going_admin_app/components/common/component_text_btn.dart';
import 'package:full_going_admin_app/config/config_color.dart';
import 'package:full_going_admin_app/config/config_style.dart';
import 'package:full_going_admin_app/enums/enum_size.dart';
import 'package:full_going_admin_app/model/admin_board/board_request.dart';
import 'package:full_going_admin_app/repository/repo_board.dart';
import 'package:full_going_admin_app/styles/style_form_decoration_full_going.dart';

import '../../../config/config_form_validator.dart';

class PageBoard extends StatefulWidget {
  const PageBoard({super.key});

  @override
  State<PageBoard> createState() => _PageBoardState();
}

/*
  "title": "string"
  "name": "string",
  "context": "string",
  "imgName": "string",
* */

class _PageBoardState extends State<PageBoard> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _setBoard(BoardRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoBoard().setBoard(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '후기 등록 완료',
        subTitle: '후기 등록이 완료되었습니다.',
      ).call();
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '후기 등록 실패',
        subTitle: '입력값을 확인해주세요.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(title: "후기 등록"),
      body: SingleChildScrollView(
        child: Container(
          padding: bodyPaddingLeftRight,
          child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const ComponentMarginVertical(enumSize: EnumSize.big),
                FormBuilderTextField(
                  name: 'title',
                  decoration:
                      StyleFormDecorationOfFullGoing().getInputDecoration('제목'),
                  maxLength: 20,
                  keyboardType: TextInputType.text,
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(
                        errorText: formErrorRequired),
                    FormBuilderValidators.minLength(3,
                        errorText: formErrorMinLength(3)),
                    FormBuilderValidators.maxLength(20,
                        errorText: formErrorMaxLength(20)),
                  ]),
                ),
                const ComponentMarginVertical(),
                FormBuilderTextField(
                  name: 'name',
                  maxLength: 20,
                  decoration: StyleFormDecorationOfFullGoing()
                      .getInputDecoration('작성자'),
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(
                        errorText: formErrorRequired),
                    FormBuilderValidators.minLength(3,
                        errorText: formErrorMinLength(3)),
                    FormBuilderValidators.maxLength(20,
                        errorText: formErrorMaxLength(20)),
                  ]),
                ),
                const ComponentMarginVertical(),
                FormBuilderTextField(
                  name: 'imgName',
                  decoration: StyleFormDecorationOfFullGoing()
                      .getInputDecoration('URL'),
                ),
                const ComponentMarginVertical(),
                FormBuilderTextField(
                  name: 'context',
                  keyboardType: TextInputType.multiline,
                  maxLines: 4,
                  decoration:
                      StyleFormDecorationOfFullGoing().getInputDecoration('내용'),
                ),
                ComponentMarginVertical(enumSize: EnumSize.big),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: ComponentTextBtn(
                      text: '등록',
                      callback: () {
                        if (_formKey.currentState!.saveAndValidate()) {
                          BoardRequest request = BoardRequest(
                            _formKey.currentState!.fields['title']!.value,
                            _formKey.currentState!.fields['name']!.value,
                            _formKey.currentState!.fields['context']!.value ?? "",
                            _formKey.currentState!.fields['imgName']!.value ?? "" ,
                          );
                          _setBoard(request);
                        }
                      }),
                ),
                const ComponentMarginVertical(),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: ComponentTextBtn(
                      text: '취소',
                      bgColor: colorSecondary,
                      borderColor: colorSecondary,
                      callback: () {
                        Navigator.pop(context);
                      }),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
