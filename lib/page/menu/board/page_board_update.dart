import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:full_going_admin_app/components/common/component_appbar_popup.dart';
import 'package:full_going_admin_app/components/common/component_custom_loading.dart';
import 'package:full_going_admin_app/components/common/component_margin_vertical.dart';
import 'package:full_going_admin_app/components/common/component_notification.dart';
import 'package:full_going_admin_app/components/common/component_text_btn.dart';
import 'package:full_going_admin_app/config/config_color.dart';
import 'package:full_going_admin_app/config/config_form_validator.dart';
import 'package:full_going_admin_app/config/config_size.dart';
import 'package:full_going_admin_app/config/config_style.dart';
import 'package:full_going_admin_app/enums/enum_size.dart';
import 'package:full_going_admin_app/model/admin_board/board_update_request.dart';
import 'package:full_going_admin_app/repository/repo_board.dart';
import 'package:full_going_admin_app/styles/style_form_decoration_full_going.dart';

class PageBoardUpdate extends StatefulWidget {
  const PageBoardUpdate({super.key, required this.boardId});

  final int boardId;

  @override
  State<PageBoardUpdate> createState() => _PageBoardUpdateState();
}

class _PageBoardUpdateState extends State<PageBoardUpdate> {
  final _formKey = GlobalKey<FormBuilderState>();

  TextEditingController _textControllerOfTitle = new TextEditingController();
  TextEditingController _textControllerOfName = new TextEditingController();
  TextEditingController _textControllerOfContext = new TextEditingController();
  TextEditingController _textControllerOfImgName = new TextEditingController();
  bool _isFixEnable = false;
  String _wroteDateTime = "";

  /*    수정
  "context": "string",
  "imgName": "string",
  "name": "string",
  "title": "string"
  * */

  /*  읽기
    "id": 0,
    "context": "string",
    "imgName": "string",
    "name": "string",
    "title": "string",

    "wroteDateTime": "string"
  * */

  Future<void> _getBoard(int boardId) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoBoard().getBoard(boardId).then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _textControllerOfTitle.text = res.data.title;
        _textControllerOfName.text = res.data.name;
        _textControllerOfContext.text = res.data.context;
        _textControllerOfImgName.text = res.data.imgName;
        _wroteDateTime = res.data.wroteDateTime;
        _isFixEnable = false;
      });
/*
      ComponentNotification(
        success: true,
        title: '킥보드 불러오기 완료',
        subTitle: '킥보드 불러오기가 완료되었습니다.',
      ).call();
  */
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '후기 불러오기 실패',
        subTitle: '확인해주세요.',
      ).call();
    });
  }

  Future<void> _putBoard(int boardId, BoardUpdateRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoBoard().putBoard(boardId, request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '후기 저장 완료',
        subTitle: '킥보드 저장 완료되었습니다.',
      ).call();
      Navigator.pop(context);
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '후기 저장 실패',
        subTitle: '입력값을 확인해주세요.',
      ).call();
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getBoard(widget.boardId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(title: "후기 상세/수정"),
      body: SingleChildScrollView(
        child: Container(
          padding: bodyPaddingLeftRight,
          child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const ComponentMarginVertical(enumSize: EnumSize.big),
                Text(
                  "작성일 : " + _wroteDateTime,
                  style: TextStyle(fontSize: fontSizeSuper),
                ),
                const ComponentMarginVertical(enumSize: EnumSize.big),
                FormBuilderTextField(
                  name: 'title',
                  controller: _textControllerOfTitle,
                  enabled: _isFixEnable,
                  decoration:
                      StyleFormDecorationOfFullGoing().getInputDecoration('제목'),
                  maxLength: 20,
                  keyboardType: TextInputType.text,
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(
                        errorText: formErrorRequired),
                    FormBuilderValidators.minLength(3,
                        errorText: formErrorMinLength(3)),
                    FormBuilderValidators.maxLength(20,
                        errorText: formErrorMaxLength(20)),
                  ]),
                ),
                const ComponentMarginVertical(),
                FormBuilderTextField(
                  name: 'name',
                  controller: _textControllerOfName,
                  enabled: _isFixEnable,
                  maxLength: 20,
                  decoration: StyleFormDecorationOfFullGoing()
                      .getInputDecoration('작성자'),
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(
                        errorText: formErrorRequired),
                    FormBuilderValidators.minLength(3,
                        errorText: formErrorMinLength(3)),
                    FormBuilderValidators.maxLength(20,
                        errorText: formErrorMaxLength(20)),
                  ]),
                ),
                const ComponentMarginVertical(),
                FormBuilderTextField(
                  name: 'imgName',
                  controller: _textControllerOfImgName,
                  enabled: _isFixEnable,
                  decoration: StyleFormDecorationOfFullGoing()
                      .getInputDecoration('URL'),
                ),
                const ComponentMarginVertical(),
                FormBuilderTextField(
                  name: 'context',
                  controller: _textControllerOfContext,
                  enabled: _isFixEnable,
                  keyboardType: TextInputType.multiline,
                  maxLines: 4,
                  decoration:
                      StyleFormDecorationOfFullGoing().getInputDecoration('내용'),
                ),
                ComponentMarginVertical(enumSize: EnumSize.big),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: ComponentTextBtn(
                    text: _isFixEnable ? "저장" : '변경',
                    borderColor: _isFixEnable ? colorGreen : colorPrimary,
                    bgColor: _isFixEnable ? colorGreen : colorPrimary,
                    callback: () {
                      setState(() {
                        if (_isFixEnable) {
                          if (_formKey.currentState!.saveAndValidate()) {
                            BoardUpdateRequest request = BoardUpdateRequest(
                              _formKey.currentState!.fields['title']!.value,
                              _formKey.currentState!.fields['name']!.value,
                              _formKey.currentState!.fields['context']!.value,
                              _formKey.currentState!.fields['imgName']!.value,
                            );

                            _putBoard(widget.boardId, request);
                          }
                        } else {
                          _isFixEnable = true;
                        }
                      });
                    },
                  ),
                ),
                const ComponentMarginVertical(),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: ComponentTextBtn(
                      text: '초기화',
                      bgColor: colorPrimary,
                      borderColor: colorPrimary,
                      callback: () {
                        _getBoard(widget.boardId);
                      }),
                ),
                const ComponentMarginVertical(),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: ComponentTextBtn(
                      text: '취소',
                      bgColor: colorSecondary,
                      borderColor: colorSecondary,
                      callback: () {
                        Navigator.pop(context);
                      }),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
