import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:full_going_admin_app/components/common/component_appbar_popup.dart';
import 'package:full_going_admin_app/components/common/component_count_title.dart';
import 'package:full_going_admin_app/components/common/component_custom_loading.dart';
import 'package:full_going_admin_app/components/common/component_no_contents.dart';
import 'package:full_going_admin_app/components/component_list_textline_item.dart';
import 'package:full_going_admin_app/model/admin_kickboard/kickboard_item.dart';
import 'package:full_going_admin_app/repository/repo_kick_board.dart';

import 'page_kick_board_update.dart';

class PageKickBoardList extends StatefulWidget {
  const PageKickBoardList({super.key});

  @override
  State<PageKickBoardList> createState() => _PageKickBoardListState();
}

class _PageKickBoardListState extends State<PageKickBoardList> {

  final _scrollController = ScrollController();

  List<KickBoardItem> _list = [];
  int _totalItemCount = 0;
  int _totalPage = 1;
  int _currentPage = 1;

  Future<void> _loadItems({bool reFresh = false}) async {
    // 새로고침하면 초기화시키기.
    if (reFresh) {
      _list = [];
      _totalItemCount = 0;
      _totalPage = 1;
      _currentPage = 1;
    }

    if (_currentPage <= _totalPage) {
      BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
        return ComponentCustomLoading(
          cancelFunc: cancelFunc,
        );
      });

      await RepoKickBoard()
          .getList(page: _currentPage)
          .then((res) =>
      {
        BotToast.closeAllLoading(),
        setState(() {
          _totalItemCount = res.totalItemCount;
          _totalPage = res.totalPage;
          _list = [..._list, ...?res.list]; // 기존 리스트에 api에서 받아온 리스트 데이터를 더하는거.
          _currentPage++;
        })
      })
          .catchError((err) =>
      {
        BotToast.closeAllLoading(),
        print(err),
      });
    }

    // 새로고침하면 스크롤위치를 맨 위로 올리기.
    if (reFresh) {
      _scrollController.animateTo(
        0,
        duration: const Duration(milliseconds: 300),
        curve: Curves.easeOut,
      );
    }
  }
  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.offset ==
          _scrollController.position.maxScrollExtent) {
        _loadItems();
      }
    });
    _loadItems();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(title: "킥보드 리스트"),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _loadItems(reFresh: true);
        },
        child: const Icon(Icons.refresh),
      ),
      body: ListView(
        controller: _scrollController,
        children: [
          _buildBody(),
        ],
      ),
    );
  }
  Widget _buildBody() {
    if (_totalItemCount > 0) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ComponentCountTitle(Icons.attachment, _totalItemCount, '건', '킥보드 리스트'),
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) =>
                ComponentListTextLineItem(
                  title: _list[index].modelName,
                  isUseContent1Line: true,
                  content1Subject: '상태',
                  content1Text: _list[index].kickBoardStatusName,
                  isUseContent2Line: true,
                  content2Subject: '위도',
                  content2Text: '${_list[index].posX}',
                  isUseContent3Line: true,
                  content3Subject: '경도',
                  content3Text: '${_list[index].posY}',
                  voidCallback: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) =>
                                PageKickBoardUpdate(kickBoardId: _list[index].kickBoardId,)));
                  },
                ),
          )
        ],
      );
    } else {
      // 이용내역이 없을때 흰화면 보이면 에러난것처럼 보이니까 이용내역 없다고 보여주기.
      return SizedBox(
        height: MediaQuery
            .of(context)
            .size
            .height - 30 - 50 - 50 - 70,
        child: GestureDetector(
          child: const ComponentNoContents(
            icon: Icons.history,
            msg: '등록된 킥보드가 없습니다.',
          ),
          onTap: () => _loadItems(reFresh: true),
        ),
      );
    }
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
}