import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:full_going_admin_app/components/common/component_appbar_popup.dart';
import 'package:full_going_admin_app/components/common/component_custom_loading.dart';
import 'package:full_going_admin_app/components/common/component_margin_vertical.dart';
import 'package:full_going_admin_app/components/common/component_notification.dart';
import 'package:full_going_admin_app/components/common/component_text_btn.dart';
import 'package:full_going_admin_app/config/config_color.dart';
import 'package:full_going_admin_app/config/config_form_validator.dart';
import 'package:full_going_admin_app/config/config_style.dart';
import 'package:full_going_admin_app/enums/enum_size.dart';
import 'package:full_going_admin_app/model/admin_kickboard/kick_board_update_request.dart';
import 'package:full_going_admin_app/repository/repo_kick_board.dart';
import 'package:full_going_admin_app/styles/style_form_decoration_full_going.dart';

class PageKickBoardUpdate extends StatefulWidget {
  const PageKickBoardUpdate({super.key, this.kickBoardId = 0});

  final int kickBoardId;

  @override
  State<PageKickBoardUpdate> createState() => _PageKickBoardUpdateState();
}

class _PageKickBoardUpdateState extends State<PageKickBoardUpdate> {
  final _formKey = GlobalKey<FormBuilderState>();
  String _initValueOfPriceBasis = "BASIC";
  String _initValueOfKickBoardStatusName = "READY";
  TextEditingController _textControllerOfModelName =
      new TextEditingController();
  TextEditingController _textControllerOfPosX = new TextEditingController();
  TextEditingController _textControllerOfPosY = new TextEditingController();
  TextEditingController _textControllerOfMemo = new TextEditingController();
  bool _isFixEnable = false;

  /*
  "modelName": "string",
  "priceBasis": "BASIC"
  "kickBoardStatusName": "string",
  "posX": 0,
  "posY": 0,
  "memo": "string",
*/

  Future<void> _getKickBoard(int kickBoardId) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoKickBoard().getKickBoard(kickBoardId).then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _textControllerOfModelName.text = res.data.modelName;
        _initValueOfPriceBasis = res.data.priceBasis;

        switch (res.data.kickBoardStatusName) {
          case "대기":
            _initValueOfKickBoardStatusName = "READY";
            break;
          case "사용중":
            _initValueOfKickBoardStatusName = "ING";
            break;
          case "점검중":
            _initValueOfKickBoardStatusName = "FIX";
            break;
        }

        _textControllerOfPosX.text = res.data.posX.toString();
        _textControllerOfPosY.text = res.data.posY.toString();
        _textControllerOfMemo.text = res.data.memo ??= "";
        _isFixEnable = false;
      });
/*
      ComponentNotification(
        success: true,
        title: '킥보드 불러오기 완료',
        subTitle: '킥보드 불러오기가 완료되었습니다.',
      ).call();
  */
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '킥보드 불러오기 실패',
        subTitle: '확인해주세요.',
      ).call();
    });
  }

  Future<void> _putKickBoard(
      int kickBoardId, String status, KickBoardUpdateRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoKickBoard()
        .putKickBoard(kickBoardId, status, request)
        .then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '킥보드 저장 완료',
        subTitle: '킥보드 저장 완료되었습니다.',
      ).call();
      Navigator.pop(context);
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '킥보드 저장 실패',
        subTitle: '입력값을 확인해주세요.',
      ).call();
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getKickBoard(widget.kickBoardId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(title: "킥보드 상세/수정"),
      body: SingleChildScrollView(
        child: Container(
          padding: bodyPaddingLeftRight,
          child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const ComponentMarginVertical(enumSize: EnumSize.big),
                FormBuilderTextField(
                  name: 'modelName',
                  enabled: _isFixEnable,
                  controller: _textControllerOfModelName,
                  decoration: StyleFormDecorationOfFullGoing()
                      .getInputDecoration('모델명'),
                  maxLength: 20,
                  keyboardType: TextInputType.text,
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(
                        errorText: formErrorRequired),
                    FormBuilderValidators.minLength(3,
                        errorText: formErrorMinLength(3)),
                    FormBuilderValidators.maxLength(20,
                        errorText: formErrorMaxLength(20)),
                  ]),
                ),
                const ComponentMarginVertical(),
                FormBuilderDropdown<String>(
                  name: 'priceBasis',
                  enabled: _isFixEnable,
                  initialValue: _initValueOfPriceBasis,
                  decoration: InputDecoration(
                    labelText: '가격기준',
                    filled: true,
                    isDense: false,
                    border: const OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                  ),
                  onChanged: (value) {
                    setState(() {});
                  },
                  items: const [
                    DropdownMenuItem(value: "BASIC", child: Text('베이직')),
                    DropdownMenuItem(value: "PREMIUM", child: Text('프리미엄')),
                  ],
                ),
                const ComponentMarginVertical(),
                FormBuilderDropdown<String>(
                  name: 'kickBoardStatusName',
                  enabled: _isFixEnable,
                  initialValue: _initValueOfKickBoardStatusName,
                  decoration: InputDecoration(
                    labelText: '킥보드 상태',
                    filled: true,
                    isDense: false,
                    border: const OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                  ),
                  onChanged: (value) {
                    setState(() {});
                  },
                  items: const [
                    DropdownMenuItem(value: "READY", child: Text('대기')),
                    DropdownMenuItem(value: "ING", child: Text('사용중')),
                    DropdownMenuItem(value: "FIX", child: Text('점검중')),
                  ],
                ),
                const ComponentMarginVertical(),
                FormBuilderTextField(
                  name: 'posX',
                  enabled: _isFixEnable,
                  controller: _textControllerOfPosX,
                  decoration:
                      StyleFormDecorationOfFullGoing().getInputDecoration('위도'),
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(
                        errorText: formErrorRequired),
                  ]),
                ),
                const ComponentMarginVertical(),
                FormBuilderTextField(
                  name: 'posY',
                  enabled: _isFixEnable,
                  controller: _textControllerOfPosY,
                  decoration:
                      StyleFormDecorationOfFullGoing().getInputDecoration('경도'),
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(
                        errorText: formErrorRequired),
                  ]),
                ),
                const ComponentMarginVertical(),
                FormBuilderTextField(
                  name: 'memo',
                  enabled: _isFixEnable,
                  controller: _textControllerOfMemo,
                  keyboardType: TextInputType.multiline,
                  maxLines: 4,
                  decoration:
                      StyleFormDecorationOfFullGoing().getInputDecoration('메모'),
                  validator: FormBuilderValidators.compose([]),
                ),
                ComponentMarginVertical(enumSize: EnumSize.big),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: ComponentTextBtn(
                    text: _isFixEnable ? "저장" : '변경',
                    borderColor: _isFixEnable ? colorGreen : colorPrimary,
                    bgColor: _isFixEnable ? colorGreen : colorPrimary,
                    callback: () {
                      setState(() {
                        if (_isFixEnable) {
                          if (_formKey.currentState!.saveAndValidate()) {
                            KickBoardUpdateRequest request =
                                KickBoardUpdateRequest(
                                    _formKey.currentState!.fields['modelName']!
                                        .value,
                                    _formKey.currentState!.fields['priceBasis']!
                                        .value,
                                    double.parse(_formKey
                                        .currentState!.fields['posX']!.value),
                                    double.parse(_formKey
                                        .currentState!.fields['posY']!.value),
                                    _formKey
                                        .currentState!.fields['memo']!.value);

                            // print("id : "+ widget.kickBoardId.toString());
                            // print(_formKey
                            //     .currentState!.fields['modelName']!.value);
                            // print(_formKey
                            //     .currentState!.fields['priceBasis']!.value);
                            // print(_formKey.currentState!.fields['posX']!.value);
                            // print(_formKey.currentState!.fields['posY']!.value);
                            // print(_formKey.currentState!.fields['memo']!.value);
                            // print("----------------------------------------------");
                            // print(_formKey.currentState!
                            //     .fields['kickBoardStatusName']!.value);

                            _putKickBoard(
                                widget.kickBoardId,
                                _formKey.currentState!
                                    .fields['kickBoardStatusName']!.value,
                                request);
                          }
                        } else {
                          _isFixEnable = true;
                        }
                      });
                    },
                  ),
                ),
                ComponentMarginVertical(),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: ComponentTextBtn(
                      text: '초기화',
                      bgColor: colorPrimary,
                      borderColor: colorPrimary,
                      callback: () {
                        _getKickBoard(widget.kickBoardId);
                      }),
                ),
                ComponentMarginVertical(),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: ComponentTextBtn(
                      text: '취소',
                      bgColor: colorSecondary,
                      borderColor: colorSecondary,
                      callback: () {
                        Navigator.pop(context);
                      }),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
