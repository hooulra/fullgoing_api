const double fontSizeSuper = 24;
const double fontSizeBig = 18;
const double fontSizeMid = 16;
const double fontSizeSm = 14;
const double fontSizeMicro = 12;

const double sizeAppBarHeight = 50;
const double sizeAppBarFilterBottomHeight = 50;
const double sizeBottomNavigationBarHeight = 50;
const double sizeAppBarLogoHeight = 25;
const double sizeAppBarLogoBizHeight = 25;
const double sizeMinWidth = 300;
const double sizeBannerTopHeight = 150;

const double sizeSuffixIconOfPassword = 18;
