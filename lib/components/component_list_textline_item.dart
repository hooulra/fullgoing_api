import 'package:flutter/material.dart';
import 'package:full_going_admin_app/components/common/component_dot.dart';
import 'package:full_going_admin_app/components/common/component_margin_vertical.dart';
import 'package:full_going_admin_app/config/config_color.dart';
import 'package:full_going_admin_app/config/config_size.dart';

class ComponentListTextLineItem extends StatelessWidget {
  const ComponentListTextLineItem({
    super.key,
    required this.title, // 얘네는 필수
    required this.voidCallback, // 얘네는 필수
    this.isUseContent1Line = false, // 언급안하면 기본값 아니오
    this.content1Subject = "", // 언급안하면 기본값 빈스트링
    this.content1Text = "", // 언급안하면 기본값 빈스트링
    this.isUseContent2Line = false, // 언급안하면 기본값 아니오
    this.content2Subject = "", // 언급안하면 기본값 빈스트링
    this.content2Text = "", // 언급안하면 기본값 빈스트링
    this.isUseContent3Line = false, // 언급안하면 기본값 아니오
    this.content3Subject = "", // 언급안하면 기본값 빈스트링
    this.content3Text = "", // 언급안하면 기본값 빈스트링
    this.isUseContent4Line = false,
    this.content4Subject = "", // 언급안하면 기본값 빈스트링
    this.content4Text = "", // 언급안하면 기본값 빈스트링
    this.isUseContent5Line = false,
    this.content5Subject = "", // 언급안하면 기본값 빈스트링
    this.content5Text = "", // 언급안하면 기본값 빈스트링
  });

  final String title; // 제목
  final bool isUseContent1Line; // 첫번째줄 사용하겠냐?
  final String content1Subject; // 첫번째줄 항목명
  final String content1Text; // 첫번째줄 내용
  final bool isUseContent2Line; // 두번째줄 사용하겠냐?
  final String content2Subject; // 두번째줄 항목명
  final String content2Text; // 두번째줄 내용
  final bool isUseContent3Line; // 세번째줄 사용하겠냐?
  final String content3Subject; // 세번째줄 항목명
  final String content3Text; // 세번째줄 내용
  final bool isUseContent4Line;
  final String content4Subject; // 세번째줄 항목명
  final String content4Text; // 세번째줄 내용
  final bool isUseContent5Line;
  final String content5Subject; // 세번째줄 항목명
  final String content5Text; // 세번째줄 내용
  final VoidCallback voidCallback; // 야 클릭됐다~~ 니가 처리해~~ 신호주기

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: voidCallback,
      child: Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.only(bottom: 10),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: colorLightGray),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              title,
              style: const TextStyle(
                fontSize: fontSizeBig,
                fontWeight: FontWeight.w700,
                wordSpacing: 1.5,
                height: 1.3,
              ),
            ),
            const ComponentMarginVertical(),
            isUseContent1Line ? Row(
              children: [
                Text(content1Subject),
                ComponentDot(),
                Text(content1Text),
              ],
            ) : Container(),
            isUseContent2Line ? Row(
              children: [
                Text(content2Subject),
                ComponentDot(),
                Text(content2Text),
              ],
            ) : Container(),
            isUseContent3Line ? Row(
              children: [
                Text(content3Subject),
                ComponentDot(),
                Text(content3Text),
              ],
            ) : Container(),
            isUseContent4Line ? Row(
              children: [
                Text(content4Subject),
                ComponentDot(),
                Text(content4Text),
              ],
            ) : Container(),
            isUseContent5Line ? Row(
              children: [
                Text(content5Subject),
                ComponentDot(),
                Text(content5Text),
              ],
            ) : Container(),
          ],
        ),
      ),
    );
  }
}

