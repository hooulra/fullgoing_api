import 'package:flutter/material.dart';
import 'package:full_going_admin_app/components/common/component_margin_horizon.dart';
import 'package:full_going_admin_app/components/common/component_margin_vertical.dart';
import 'package:full_going_admin_app/config/config_color.dart';
import 'package:full_going_admin_app/config/config_size.dart';
import 'package:full_going_admin_app/config/config_style.dart';
import 'package:full_going_admin_app/enums/enum_size.dart';

class ComponentBoxFullGoingPassState extends StatelessWidget {
  const ComponentBoxFullGoingPassState({
    super.key,
    required this.title, // 얘네는 필수
    required this.sub1Text,
    required this.sub2Text,
    required this.sub3Text,
    required this.sub4Text,
    required this.voidCallback,
  });

  final String title; // 제목
  final String sub1Text;
  final String sub2Text;
  final String sub3Text;
  final String sub4Text; // 밑줄 금액

  final VoidCallback voidCallback; // 야 클릭됐다~~ 니가 처리해~~ 신호주기

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        margin: contentMarginLeftRightOfFullGoingPassBox,
        padding: contentPaddingLeftRightOfFullGoingPassBox,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            border: Border.all(color: Colors.lightBlue, width: 2),
            color: Colors.white),
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(title,
                    style: TextStyle(
                        fontSize: 25, fontWeight: FontWeight.w600)),
                ComponentMarginVertical(enumSize: EnumSize.micro),
                Text(sub1Text, style: TextStyle(fontSize: fontSizeMid)),
                ComponentMarginVertical(),
                Row(
                  children: [
                    Text(
                      sub2Text,
                      style: TextStyle(
                          fontSize: fontSizeSuper, color: colorTeal),
                    ),
                    ComponentMarginHorizon(enumSize: EnumSize.big),
                    Text(sub3Text,
                        style: TextStyle(fontSize: fontSizeSuper)),
                  ],
                ),
                ComponentMarginVertical(enumSize: EnumSize.micro),
                Text(sub4Text,
                    style: TextStyle(
                        color: colorDarkGray,
                        decoration: TextDecoration.lineThrough)),
              ],
            ),
            SizedBox(
                child: Icon(Icons.arrow_forward_ios),
              ),
          ],
        ),
      ),
      onTap: voidCallback,
    );
  }
}
