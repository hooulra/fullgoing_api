import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:full_going_admin_app/components/common/component_notification.dart';
import 'package:full_going_admin_app/page/member/page_login.dart';
import 'package:shared_preferences/shared_preferences.dart';


class TokenLib {
  static Future<String?> getMemberToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('memberToken');
  }

  static Future<String?> getMemberName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('memberName');
  }

  static void setMemberToken(String memberToken) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('memberToken', memberToken);
  }

  static void setMemberName(String memberName) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('memberName', memberName);
  }

//test code
  static Future<String?> getMemberProfileImgPath() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('memberProfileImgPath');
  }

//test code
  static void setMemberProfileImgPath(String imgPath) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('memberProfileImgPath', imgPath);
  }



  static void logout(BuildContext context) async {
    BotToast.closeAllLoading();

    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();

    ComponentNotification(
      success: false,
      title: '로그아웃',
      subTitle: '로그아웃되어 로그인 페이지로 이동합니다.',
    ).call();

    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (BuildContext context) => const PageLogin()),
            (route) => false
    );
  }
}
