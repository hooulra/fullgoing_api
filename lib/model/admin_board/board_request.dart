class BoardRequest {
  String title;
  String name;
  String? context;
  String? imgName;

  /*
  "context": "string",
  "imgName": "string",
  "name": "string",
  "title": "string"
 */


  BoardRequest(this.title, this.name, this.context, this.imgName);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['title'] = title;
    data['name'] = name;
    data['context'] = context;
    data['imgName'] = imgName;

    return data;
  }
}