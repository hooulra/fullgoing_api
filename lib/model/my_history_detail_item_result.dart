import 'package:full_going_admin_app/model/my_history_detail_item.dart';

class MyHistoryDetailItemResult {
  int code;
  bool isSuccess;
  String msg;
  MyHistoryDetailItem data;

  MyHistoryDetailItemResult(this.code, this.isSuccess, this.msg, this.data);

  factory MyHistoryDetailItemResult.fromJson(Map<String, dynamic> json) {
    return MyHistoryDetailItemResult(
      json['code'],
      json['isSuccess'],
      json['msg'],
      MyHistoryDetailItem.fromJson(json['data']),
    );
  }
}