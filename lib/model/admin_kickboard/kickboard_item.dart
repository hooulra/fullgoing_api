class KickBoardItem {
  int kickBoardId;
  String kickBoardStatusName;
  String modelName;
  double posX;
  double posY;

  KickBoardItem(this.kickBoardId, this.kickBoardStatusName, this.modelName, this.posX, this.posY);

  factory KickBoardItem.fromJson(Map<String, dynamic> json) {
    return KickBoardItem(
      json['kickBoardId'],
      json['kickBoardStatusName'],
      json['modelName'],
      json['posX'],
      json['posY'],
    );
  }
}
