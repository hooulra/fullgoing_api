
class UserItem {
  int memberId;
  String dateCreate;
  String username;
  String name;
  String phoneNumber;
  bool isLicence;

  UserItem(this.memberId, this.dateCreate, this.username, this.name, this.phoneNumber, this.isLicence);

  factory UserItem.fromJson(Map<String, dynamic> json) {
    return UserItem(
        json['memberId'],
        json['dateCreate'],
        json['username'],
        json['name'],
        json['phoneNumber'],
        json['isLicence'],
    );
  }

}