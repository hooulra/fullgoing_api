class MyHistoryDetailItem {
  String dateEnd;
  String dateStart;
  String kickBoardName;
  num resultPass;
  num resultPrice;

  MyHistoryDetailItem(this.dateEnd, this.dateStart, this.kickBoardName, this.resultPass, this.resultPrice);

  factory MyHistoryDetailItem.fromJson(Map<String, dynamic> json) {
    return MyHistoryDetailItem(
      json['dateEnd'],
      json['dateStart'],
      json['kickBoardName'],
      json['resultPass'],
      json['resultPrice'],
    );
  }
}
