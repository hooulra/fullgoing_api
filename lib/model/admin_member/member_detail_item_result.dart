import 'package:full_going_admin_app/model/admin_member/member_detail_item.dart';

class MemberDetailItemResult {
  int code;
  bool isSuccess;
  String msg;
  MemberDetailItem data;

  MemberDetailItemResult(this.code, this.isSuccess, this.msg, this.data);

  factory MemberDetailItemResult.fromJson(Map<String, dynamic> json) {
    return MemberDetailItemResult(
      json['code'],
      json['isSuccess'],
      json['msg'],
      MemberDetailItem.fromJson(json['data']),
    );
  }
}